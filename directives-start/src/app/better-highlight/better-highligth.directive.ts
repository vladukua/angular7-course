import { Directive, Renderer2, OnInit, ElementRef, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBetterHighligth]'
})
export class BetterHighligthDirective implements OnInit {
  @Input()
  defaultColor: string = 'transparent';

  @Input('appBetterHighligth')
  highligthColor: string = 'blue';

  @HostBinding('style.backgroundColor')
  backgroundColor: string;
  
  constructor(
    private elRef: ElementRef, 
    private renderer: Renderer2) { }

  ngOnInit() {
    //this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
    this.backgroundColor = this.defaultColor;
  }

  @HostListener('mouseenter') 
  mouseOver(eventData: Event) {
    //this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
    this.backgroundColor = this.highligthColor;
  }

  @HostListener('mouseleave') 
  mouseLeave(eventData: Event) {
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');
    this.backgroundColor = this.defaultColor;
  }
}
