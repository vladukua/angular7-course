import { Component } from '@angular/core';
import { Logs } from 'selenium-webdriver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public displayParagraph: boolean = true;
  public logs: String[] = [];

  public onDisplayDetails(): void {
    this.displayParagraph = !this.displayParagraph;

    this.logs.push("You clicked 'Display Details' button at " + new Date().toLocaleString());
  }
}
