import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';

// @Injectable({ providedIn: 'root' })
export class RecipeService {

    private recipes: Recipe[] = [
        new Recipe('Snitzel', 'Tasty snitzel.', 
        'https://www.savoredjourneys.com/wp-content/uploads/2015/10/schnitzel-germany.jpg',
        [
            new Ingredient('Meat', 1),
            new Ingredient('French Fries', 20)
        ]),
        new Recipe('Burger', 'Big burger.', 
        'https://res.cloudinary.com/hellofresh/image/upload/f_auto,fl_lossy,q_auto/v1/hellofresh_s3/image/beef-burger-with-caramelised-onion-jam-d6d4260d.jpg',
        [
            new Ingredient('Buns', 2),
            new Ingredient('Meat', 1)
        ])
      ];

      getRecipes(): Recipe[] {
          return this.recipes.slice();
      }

      getRecipe(id: number): Recipe {
          return this.recipes[id];
      }
}