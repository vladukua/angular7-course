import { Directive, HostListener, Renderer2, ElementRef, HostBinding } from '@angular/core';

@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective {
    @HostBinding('class.open') opened = false;

    @HostListener('click')
    toggleOpen() {
        this.opened = ! this.opened;
        // if (this.opened) {
        //     this.renderer.addClass(this.elementRef.nativeElement, 'open');
        // } else {
        //     this.renderer.removeClass(this.elementRef.nativeElement, 'open');
        // }
    }

    // constructor(private elementRef: ElementRef, private renderer: Renderer2) {}
}