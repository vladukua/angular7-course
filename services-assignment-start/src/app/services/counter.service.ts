import { Injectable } from "@angular/core";

@Injectable({providedIn: 'root'})
export class CounterService {
    public count: number = 0;

    public incrementCount() : void {
        this.count++;
    }
}